// V tej datoteki je opredeljen objekt Klepet, 
// ki nam olajša obvladovanje funkcionalnosti uporabnika


var Klepet = function(socket) {
  this.socket = socket;
  this.users  = {};
};


/**
 * Pošiljanje sporočila od uporabnika na strežnik z uporabo 
 * WebSocket tehnologije po socketu 'sporocilo'
 * 
 * @param kanal ime kanala, na katerega želimo poslati sporočilo
 * @param besedilo sporočilo, ki ga želimo posredovati
 */
Klepet.prototype.posljiSporocilo = function(kanal, besedilo) {
  var sporocilo = {
    kanal: kanal,
    besedilo: besedilo
  };
  this.socket.emit('sporocilo', sporocilo);
};


Klepet.prototype.krcniUporabnika = function(vzdevek){

  var krc = {
    vzdevek: vzdevek,
    krc: true,
    besedilo: "Krc"
  }
  this.socket.emit('sporocilo', krc);

}

/**
 * Uporabnik želi spremeniti kanal, kjer se zahteva posreduje na strežnik 
 * z uporabo WebSocket tehnologije po socketu 'pridruzitevZahteva'
 * 
 * @param kanal ime kanala, kateremu se želimo pridružiti
 */
Klepet.prototype.spremeniKanal = function(kanal) {
  this.socket.emit('pridruzitevZahteva', {
    novKanal: kanal
  });
};


/**
 * Procesiranje ukaza, ki ga uporabnik vnese v vnosno polje, kjer je potrebno
 * najprej izluščiti za kateri ukaz gre, nato pa prebrati še parametre ukaza
 * in zahtevati izvajanje ustrezne funkcionalnosti
 * 
 * @param ukaz besedilo, ki ga uporabnik vnese v vnosni obrazec na spletu
 */
Klepet.prototype.procesirajUkaz = function(ukaz) {
  var besede = ukaz.split(' ');
  // Izlušči ukaz iz vnosnega besedila
  ukaz = besede[0].substring(1, besede[0].length).toLowerCase();
  var sporocilo = false;

  switch(ukaz) {
    case 'barva':
      besede.shift();
      nastaviBarvo(besede[0]);
      sporocilo = "Nastavil si si barvo na "+besede[0];
      break;
    case 'pridruzitev':
      besede.shift();
      var kanal = besede.join(' ');
      // Sproži spremembo kanala
      this.spremeniKanal(kanal);
      break;

    case 'preimenuj':

      besede.shift();
      var vzdevek    = besede[0].split('"')[1];

      var temp = pridobiVzdevekIzNadimka(vzdevek);
      if(temp) vzdevek = temp;

      var novVzdevek = besede[1].split('"')[1];

      if(users[vzdevek]){
        customNicks[users[vzdevek].id] = novVzdevek; 
        sporocilo = "Uspešno si preimenoval uporabnika.";
      }else{
        sporocilo = "Ne moreš preimenovati tega uporabnika.";
      }

    break;
    case 'vzdevek':
      besede.shift();
      var vzdevek = besede.join(' ');
      // Zahtevaj spremembo vzdevka
      this.socket.emit('vzdevekSpremembaZahteva', vzdevek);
      break;
    case 'zasebno':
      besede.shift();
      var besedilo = besede.join(' ');
      var parametri = besedilo.split('\"');
      if (parametri) {
        
        var vzdevek = parametri[1];

        var real = pridobiVzdevekIzNadimka(vzdevek);
        if(real) vzdevek = real;

        this.socket.emit('sporocilo', {vzdevek: vzdevek, besedilo: parametri[3]});

        var temp = pridobiNadimekIzVzdevka(vzdevek);
        if(temp) vzdevek = temp;
        
        sporocilo = '(zasebno za ' + vzdevek + '): ' + parametri[3];
      
      } else {
        sporocilo = 'Neznan ukaz';
      }
      break;

    default:
      // Vrni napako, če pride do neznanega ukaza
      sporocilo = 'Neznan ukaz.';
      break;
  };

  return sporocilo;
};
