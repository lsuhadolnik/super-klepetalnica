/**
 * Zamenjava kode smeška s sliko (iz oddaljenega strežnika
 * https://sandbox.lavbic.net/teaching/OIS/gradivo/{smesko}.png)
 * 
 * @param vhodnoBesedilo sporočilo, ki lahko vsebuje kodo smeška
 */

var users = {};

var customNicks = {};

function pridobiVzdevekIzNadimka(nadimek){

  for(var kl in customNicks){

    if(customNicks[kl] == nadimek){
      console.log(kl);
      var vz = pridobiVzdevekIzIDja(kl);
        return vz;
      
    }

  }

  return false;

}

function pridobiNadimekIzVzdevka(vzdevek){
 
  var us = users[vzdevek]
  if(us) return customNicks[us.id];

  return false;

}

function pridobiVzdevekIzIDja(id){

  id = parseInt(id);

  for(var kl in users){
    if(users[kl].id == id)
      return kl;
  }

  return false;

}

function posodobiUporabnike(uporabniki){

//    console.log(uporabniki)

    users = {};

    uporabniki.forEach(function(user){

      var id  = user.id;
      var ime = user.ime;

      var podatki = {}

      podatki.id  = id;
      podatki.ime = ime;

      users[ime] = podatki;

    });

}

function posodobiSeznamUporabnikov(klepetApp, uporabniki){

    posodobiUporabnike(uporabniki);
  
    $('#seznam-uporabnikov').empty();
    for(var userID in users) {
      var cust = customNicks[users[userID].id]; 
      var ime = cust ? cust+' ('+userID+')' : userID;
      var el = divElementEnostavniTekst(ime);
      $(el).attr("Username",users[userID].ime);
     
      $(el).on("click", function(e){
           
              var u = e.target.getAttribute("Username");
              
              klepetApp.krcniUporabnika(u);
              $('#sporocila').append(divElementEnostavniTekst("Krcnil si uporabnika "+u));

      });
     
      $('#seznam-uporabnikov').append(el);
    }

}

function dodajSmeske(vhodnoBesedilo) {
  var preslikovalnaTabela = {
    ";)": "wink.png",
    ":)": "smiley.png",
    "(y)": "like.png",
    ":*": "kiss.png",
    ":(": "sad.png"
  }
  for (var smesko in preslikovalnaTabela) {
    vhodnoBesedilo = vhodnoBesedilo.split(smesko).join(
      "<img src='https://sandbox.lavbic.net/teaching/OIS/gradivo/" +
      preslikovalnaTabela[smesko] + "' />");
  }
  return vhodnoBesedilo;
}

function pridobiURLje(besedilo){

  var urls = besedilo.match(/(http(?:s)?\:\/\/[^\s]+\.(?:png|jpg|gif))/g);
  
  return urls ? urls : [];

}


/**
 * Čiščenje besedila sporočila z namenom onemogočanja XSS napadov
 * 
 * @param sporocilo začetno besedilo sporočila
 */
function divElementEnostavniTekst(sporocilo) {
  var jeSmesko = sporocilo.indexOf("https://sandbox.lavbic.net/teaching/OIS/gradivo/") > -1;
  if (jeSmesko) {
    sporocilo = 
      sporocilo.split("<").join("&lt;")
               .split(">").join("&gt;")
               .split("&lt;img").join("<img")
               .split("png' /&gt;").join("png' />");
    return divElementHtmlTekst(sporocilo);
  } else {
    return $('<div style="font-weight: bold"></div>').text(sporocilo);  
  }
}


/**
 * Prikaz "varnih" sporočil, t.j. tistih, ki jih je generiral sistem
 * 
 * @param sporocilo začetno besedilo sporočila
 */
function divElementHtmlTekst(sporocilo) {
  return $('<div></div>').html('<i>' + sporocilo + '</i>');
}

function divSlika(url) {
  return $('<div></div>').html('<img style="width:200px; margin-left:20px;" src="' + url + '">');
}

function divSlike(urls){
  if(urls && urls.length > 0)
    return urls.map(function(url){return "<img style='width:200px; margin-left:20px;' src='"+url+"'>"}).join("<br />");
  return $("<div></div>");
}


function nastaviBarvo(barva){

  $("#kanal, #sporocila").css("background-color", barva);

}



/**
 * Obdelaj besedilo, ki ga uporabnik vnese v obrazec na spletni strani, kjer
 * je potrebno ugotoviti ali gre za ukaz ali za sporočilo na kanal
 * 
 * @param klepetApp objekt Klepet, ki nam olajša obvladovanje 
 *        funkcionalnosti uporabnika
 * @param socket socket WebSocket trenutno prijavljenega uporabnika
 */
function procesirajVnosUporabnika(klepetApp, socket) {
  var sporocilo = $('#poslji-sporocilo').val();
  
  var imageURLS = pridobiURLje(sporocilo);
 
  sporocilo = dodajSmeske(sporocilo);
  
  
  var sistemskoSporocilo;
  
  // Če uporabnik začne sporočilo z znakom '/', ga obravnavaj kot ukaz
  if (sporocilo.charAt(0) == '/') {
    sistemskoSporocilo = klepetApp.procesirajUkaz(sporocilo);
    if (sistemskoSporocilo) {
      $('#sporocila').append(divElementHtmlTekst(sistemskoSporocilo));
    }
  // Če gre za sporočilo na kanal, ga posreduj vsem članom kanala
  } else {
    sporocilo = filtirirajVulgarneBesede(sporocilo);
    klepetApp.posljiSporocilo(trenutniKanal, sporocilo);
    $('#sporocila').append(divElementEnostavniTekst(sporocilo))
                   
    if(imageURLS.length > 0)
    $("#sporocila").append(divSlike(imageURLS));
    

    $('#sporocila').scrollTop($('#sporocila').prop('scrollHeight'));
  }

  $('#poslji-sporocilo').val('');
}

// Branje vulgarnih besed v seznam
var vulgarneBesede = [];
$.get('/swearWords.txt', function(podatki) {
  vulgarneBesede = podatki.split('\r\n'); 
});

/**
* Iz podanega niza vse besede iz seznama vulgarnih besed zamenjaj z enako dolžino zvezdic (*).
* 
* @param vhodni niz
*/
function filtirirajVulgarneBesede(vhod) {

 for (var i in vulgarneBesede) {
   var re = new RegExp('\\b' + vulgarneBesede[i] + '\\b', 'gi');
   vhod = vhod.replace(re, "*".repeat(vulgarneBesede[i].length));
 }
 
 return vhod;
}



function vpisiVBesedilnoOkno(novElement){
    $('#sporocila').append(novElement);
}



var socket = io.connect();
var trenutniVzdevek = "";
var trenutniKanal = "";

// Poačakj, da se celotna stran naloži, šele nato začni z izvajanjem kode
$(document).ready(function() {
  var klepetApp = new Klepet(socket);
  
  // Prikaži rezultat zahteve po spremembi vzdevka
  socket.on('vzdevekSpremembaOdgovor', function(rezultat) {
    var sporocilo;
    if (rezultat.uspesno) {
      trenutniVzdevek = rezultat.vzdevek;
      $('#kanal').text(trenutniVzdevek + " @ " + trenutniKanal);
      sporocilo = 'Prijavljen si kot ' + rezultat.vzdevek + '.';
    } else {
      sporocilo = rezultat.sporocilo;
    }
    $('#sporocila').append(divElementHtmlTekst(sporocilo));
  });
  
  // Prikaži rezultat zahteve po spremembi kanala
  socket.on('pridruzitevOdgovor', function(rezultat) {
    trenutniKanal = rezultat.kanal;
    $('#kanal').text(trenutniVzdevek + " @ " + trenutniKanal);
    $('#sporocila').append(divElementHtmlTekst('Sprememba kanala.'));
  });
  
  // Prikaži prejeto sporočilo
  socket.on('sporocilo', function (sporocilo) {
    
    var novElement = divElementEnostavniTekst(sporocilo.besedilo);
    
    if(sporocilo.krc){

      novElement = divElementHtmlTekst(sporocilo.besedilo+" (zasebno) &#9756;");

    }
    
    if(sporocilo.tip){

      if(sporocilo.tip == 'msg'){
        var tekst = customNicks[sporocilo.id] ? customNicks[sporocilo.id] : sporocilo.od;
        
        if(sporocilo.zasebno){
          tekst += " (zasebno)";
        }

          tekst += ": "+sporocilo.golo;
          novElement = divElementEnostavniTekst(tekst);
        }

        if(sporocilo.tip == 'spremembaVzdevka'){
          
          
          var tekst = customNicks[sporocilo.id] ? customNicks[sporocilo.id] : sporocilo.od;

          tekst += " se je preimenoval v "+sporocilo.novoIme+".";

          novElement = divElementEnostavniTekst(tekst);
        }

      }

    $('#sporocila').append(novElement);
    
    var imageURLS = pridobiURLje(sporocilo.besedilo);

    if(imageURLS.length > 0)
    $("#sporocila").append(divSlike(imageURLS));

  });
  
  // Prikaži seznam kanalov, ki so na voljo
  socket.on('kanali', function(kanali) {
    $('#seznam-kanalov').empty();

    for(var i in kanali) {
      if (kanali[i] != '') {
        $('#seznam-kanalov').append(divElementEnostavniTekst(kanali[i]));
      }
    }
  
    // Klik na ime kanala v seznamu kanalov zahteva pridružitev izbranemu kanalu
    $('#seznam-kanalov div').click(function() {
      klepetApp.procesirajUkaz('/pridruzitev ' + $(this).text());
      $('#poslji-sporocilo').focus();
    });
  });
  
  // Prikaži seznam trenutnih uporabnikov na kanalu
  socket.on('uporabniki', function(uporabniki) {
    
    posodobiSeznamUporabnikov(klepetApp, uporabniki);

  });
  
  // Seznam kanalov in uporabnikov posodabljaj vsako sekundo
  setInterval(function() {
    socket.emit('kanali');
    socket.emit('uporabniki', {kanal: trenutniKanal});
  }, 1000);

  $('#poslji-sporocilo').focus();
  
  // S klikom na gumb pošljemo sporočilo strežniku
  $('#poslji-obrazec').submit(function() {
    procesirajVnosUporabnika(klepetApp, socket);
    return false;
  });
});
